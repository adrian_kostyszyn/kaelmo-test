<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->render('index');
    }

   
    
    
    public function actionWyniki() {
             $Dane = new CActiveDataProvider('osobyFormularz', array(
                 'Pagination' => array (
                  'PageSize' =>5 
              ),
             ));

  $this->render('wyniki',array(
      'Dane'=>$Dane,
  ));
    }

    public function actionAnkieta() {        
                $Modelosoby = new osobyFormularz;
        if(isset($_POST['osobyFormularz'])){
            $Modelosoby->attributes = $_POST['osobyFormularz'];
            if($Modelosoby->validate()){
                if($Modelosoby->obywatelstwo!=='')
                                $Modelosoby->obywatelstwo=implode(',',$Modelosoby->obywatelstwo);//converting to string...
                if($Modelosoby->edukacja!=='')
                                $Modelosoby->edukacja=implode(',',$Modelosoby->edukacja);
                if ($Modelosoby->save())
                    $this->redirect('index.php?r=site/wyniki');

            }
        }
        $this->render('formularz', array(
            'model'=>$Modelosoby,
        ));
        
        
        
        
    }

}
?>