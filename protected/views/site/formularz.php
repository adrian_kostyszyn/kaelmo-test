<div class="form" style="padding: 15px;">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'formularz',
    'enableAjaxValidation'=>false,)); ?>
<p class="note">Pola oznaczone<span class="required">*</span> sa wymagane.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <?php echo $form->labelEx($model,'imie'); ?>
    <?php echo $form->textField($model,'imie'); ?>
    <?php echo $form->error($model,'imie'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'nazwisko'); ?>
    <?php echo $form->textField($model,'nazwisko'); ?>
    <?php echo $form->error($model,'nazwisko'); ?>
</div>
<div><b>Płeć :</b></div>
<div class="row">
    <?php echo $form->radioButton($model,'plec',
            array(
        'man'=> 'mężczyzna',
    )); ?> Mężczyzna
</div>

<div class="row">
    <?php echo $form->radioButton($model,'plec',
            array(
        'woman'=> 'kobieta',
    )); ?> Kobieta
</div>


<div><b>Obywatelstwo :</b></div>

<?php 
echo CHtml::activeCheckboxList(
  $model, 'obywatelstwo', 
  array('pl'=>'Polskie', 'ang'=>'Angielskie','inne'=>'Inne'),
  array('template'=>'<li>{input} {label}</li>',)
);
?>

<div><b>Edukacja :</b></div>

<?php 
echo CHtml::activeCheckboxList(
  $model, 'edukacja', 
  array('pod'=>'Podstawowe', 'mid'=>'Średnie','high'=>'Wyższe'),
  array('template'=>'<li>{input} {label}</li>',)
);
?>


<div class="row buttons">
    <?php echo CHtml::submitButton('Dodaj'); ?>
</div>

<?php $this->endWidget(); ?>
</div>