<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of osobyFormularz
 *                                                                                                                                                                       
 * @author kostyszyn
 */
class osobyFormularz extends CActiveRecord{
    public $id;
    public $imie;
    public $nazwisko;
    public $plec;
    public $edukacja;
    public $obywatelstwo;
    
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
    
    public function tableName(){
        return 'osoby';
    }
    
    public function attributeLabels(){
        return array(
            'id'=>'ID',
            'imie'=>'Imię',
            'nazwisko'=>'Nazwisko',
            'plec'=>'Płeć',
            'edukacja'=>'Edukacja',
            'obywatelstwo'=>'Obywatelstwo'
        );
    }
    
    public function rules(){
        return array(
            array('imie',  'required'),
            array('nazwisko',  'required'),
            array('plec',  'required'),
            array('edukacja',  'required'),
            array('obywatelstwo',  'required'),
        );
    }
    
    
}
